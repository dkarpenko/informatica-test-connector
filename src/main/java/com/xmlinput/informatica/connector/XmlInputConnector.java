package com.xmlinput.informatica.connector;

import com.informatica.cloud.api.adapter.common.OperationContext;
import com.informatica.cloud.api.adapter.connection.ConnectionFailedException;
import com.informatica.cloud.api.adapter.connection.InsufficientConnectInfoException;
import com.informatica.cloud.api.adapter.metadata.MetadataReadException;
import com.informatica.cloud.api.adapter.metadata.RecordInfo;
import com.informatica.connector.wrapper.util.BackendFieldInfo;
import com.informatica.connector.wrapper.util.BackendObjectInfo;
import com.informatica.connector.wrapper.util.ISimpleConnector;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.xml.stream.XMLInputFactory;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import static java.util.Collections.emptyList;

public class XmlInputConnector extends ISimpleConnector {

    private static final Log LOGGER = LogFactory.getLog(XmlInputConnector.class);

    private static final String PLUGIN_UUID = "1937704c-36fc-43ee-bae0-cd5d3b65d229";

    public String connectionUrl = null;

    @Override
    public boolean connect() throws InsufficientConnectInfoException, ConnectionFailedException {

        XMLInputFactory factory = org.apache.cxf.staxutils.StaxUtils.createXMLInputFactory(true);

        LOGGER.info(factory.getClass().getCanonicalName());
        return true;
    }

    @Override
    public boolean disconnect() {
        return true;
    }

    @Override
    public UUID getPluginUUID() {
        return UUID.fromString(PLUGIN_UUID);
    }

    @Override
    public List<BackendObjectInfo> getObjectList(Pattern pattern, OperationContext operationContext) throws MetadataReadException {
        return emptyList();
    }

    @Override
    public List<BackendFieldInfo> getFieldList(RecordInfo recordInfo, OperationContext operationContext) throws MetadataReadException {
        return emptyList();
    }

    public String getConnectionUrl() {
        return connectionUrl;
    }

    public void setConnectionUrl(String connectionUrl) {
        this.connectionUrl = connectionUrl;
    }
}
