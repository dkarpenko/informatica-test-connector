package com.xmlinput.informatica.connector;

import com.informatica.cloud.api.adapter.plugin.PluginVersion;
import com.informatica.connector.wrapper.plugin.InfaPlugin;

public class XmlInputConnectorDescriptor extends InfaPlugin {

    private static final int MAJOR_VERSION = 1;
    private static final int MINOR_VERSION = 0;
    private static final int BUILD_VERSION = 0;

    public XmlInputConnectorDescriptor() {
        super(new XmlInputConnector());
    }

    @Override
    public PluginVersion getVersion() {
        return new PluginVersion(MAJOR_VERSION, MINOR_VERSION, BUILD_VERSION);
    }

}
